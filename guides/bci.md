# Отглеждане на Боа Констриктор Император (Boa Constrictor Imperator)

## Въведение

## Факти

![boa](@assets/guides/boa/boa_thumb.jpg)

* **Тип:** Влечуго (Reptile) 
* **Семейство:** Boidae 
* **Регион:** От Мексико до Централна и Южна Америка 
* **Размери:** от 1 до 3.5 метра (в зависимост от региона им) 
* **Терариум:** 1.2 - 0.6 - 0.4 м. д/ш/в 
* **Храна:** Хищник (гризачи, птици, зайци) 
* **Средна продължителност на живот:** 20-30 години 
* **Тегло:** 22-30 кг 
* **Температура:** 26-29 С дневна, с 32 градуса максимална за препичане, нощна до 22 С

Боите са неотровни удушвачи намиращи се в тропическите части на Централна и Южна Америка. Като своите братовчеди анакондите, те са прекрасни плувци но предпочитат да стоят на сушата, живеейки в кухи дънери и дупки в земята. 
Сравнително по-малки от анакондите, боите растат до 3-4 (отделни екземпляри) метра и могат да тежат до 45 килограма. Техните челюсти са пълни с малки кукообразни зъби, предназначени за захващане на плячката, докато те се увиват около нея, стискайки ги до удушаване и смърт. Боите се хранят с почти всичко което могат да хванат, включително птици, маймуни и диви прасета. 


## Съдържание на наръчника

[[toc]]

## Размери на Боа Констриктор

![Възрастна Боа Констриктор](@assets/guides/boa/BigBoa.jpg)

Боите са средно големи змии, достигащи до размери от 2.5 -3 метра, някой локалности до 4. Те са с големи, мускулести тела и една женска може да тежи към 30-40 кг. Женските екземпляри са сравнително по-големи от мъжките. Типичен мъжки е между 1.9-2.2 метра до 2.5, докато една женска може да стигне над 3.5 метра. Като малки са около 40 см но много бързо растат и до края на първата си годинка могат да станат около 90см, растейки до внушителни размери с годините.

> Змиите забавят растежа си след първите няколко години, но никога не спират да растат. Растежа им се определя от това колко се хранят и колко често.

## Терариум за Боа Констриктор

Боите са големи змии, с дебели и мускулести тела, това означава че трябва да им се предостави голям терариум. Има много видове терариуми, от стъклени с предни отварящи се врати, големи аквариуми с капаци, дървени терариуми с врати отпред, до пластмасови терариуми. 

Един типичен терариум за боа би бил с размери: 1.2 метра дълъг, 60 см широк и поне 40 висок. Ако и направите по-голям, животното няма да се сърди, но това е минимума който трябва да предоставите на възрастно животно. Малките екземпляри може да бъдат гледани в малки терариуми, като простото правило е : дължината + ширината на терариума = дължината на змията, това правило важи за малки животни, имайки в предвид колко бързо растат, по-голям терариум от малки, по-добре. На кратко, за боа до 1.2 метра може да използвате терариум 70 см на 40 см, като след това, на животното ще му става все по-тясно и трябва да мислите за по-голям терариум.


## Видове Терариуми за Боа Констриктор

### Стъклени терариуми

![Стъклен терариум ::подходящ за малки бои](@assets/guides/boa/glass_viv.jpg)

Стъклените терариуми на пръв поглед изглеждат евтини и лесно достъпни. В България, голяма част от хората предпочитат да гледат своите животни в стъклени терариуми, но дали те са най-подходящи. На пръв поглед един малък стъклен терариум е хубав, животното се вижда от всякъде и може да му се радвате, не се повреждат от влага, но плюсовете им се изчерпват тук. Стъклените терариуми се отопляват много по-трудно от другите видове, изстиват много лесно, задържат влага по-трудно, тежки са и са чупливи. 1.2 метра на 60 см стъклен терариум би бил прекалено труден за местене и почистване.


### Аквариуми

![Аквариум пригоден за боа::Не препоръчваме отглеждането на бои в аквариуми](@assets/guides/boa/tank.jpg)

Това е просто временно решение, което не предлагам на никои. Вентилацията им е неефективна, характеристиките им са като на стъкленият терариум, а факта че трябва да посягате отгоре на змията прави ситуацията пъти по-лоша. Те приемат като заплаха посягането от отгоре, защото в природата по този начин ги нападат хищници, това би довело до агресия и стрес на животното. 


### Дървени терариуми

![Дървен терариум::декориран с 3D декор](@assets/guides/boa/wood.jpg)

Дървените терариуми са един от най-добрите избори за терариум за боа. Дървото е лош проводник, което означава че ще задържа топлината добре. Вентилацията им е добра, в зависимост от разположението на вентилационните отвори. Те задържат влагата много добре. При правилна изработка, тези терариуми не се влияят особено от влагата в терариума. Здрави, те могат да се складират един над друг, спестявайки място в помещението. Могат лесно да се модифицират, да се закрепват към тях други аксесоари, даже и терариуми.


### Пластмасови

![Пластмасов терариум за големи змии](@assets/guides/boa/plasticcage.jpg)

Това са нов вид терариуми, още не са навлезли в България. Те имат най-много предимства пред всички други видове. Те са най-леки, най-здрави, имат абсолютна влаго издръжливост. Вентилацията, способността да задържат влага и температура са също много добри. Леки и лесни за почистване, те могат да се складират един над друг. За жалост, те почти не се срещат в нашата държава в момента на писането на този наръчник.


## Отопление на Боа Констриктор

Това е може би най-важната част от наръчника ако искате вашето животно да живее дълъг и пълноценен живот. Тези животни са тропически, затова трябва да се държат в тропическите 25-32 градуса. Място за препичане около 32 градуса също е хубаво да бъде предоставено. По този начин животното ще може да регулира температурата на тялото си.  

За да няма объркване, околната среда трябва да е в рамките на 25-30 градуса, това е цялостната температура на терариума. 32 градуса трябва да е температурата под(над) самият нагревателен елемент.  Температурата се измерва на земята на терариума.
Освен ако не размножавате боите, тази температура трябва да се поддържа целогодишно. Когато вадите животното то не трябва да е студено, а малко по-хладно от вашите ръце. Ако човешката температура е около 37 градуса, то тази на животното трябва да е около 28. 
Ако имате повече от един нагревател, то те трябва да са в една и съща част на терариума, за да се поддържа температурният градиент. 


### Видове нагреватели
Има много видове нагреватели, които може да използвате за отопление, и това до голяма част зависи от вида на терариума, които имате.

![Нагревателно килимче за терариум](http://terrariums.eu/e107_plugins/easyshop/product_images_acc/heat_mats/heat_mat_25x50_2.jpg)

#### Нагревателно килимче

Това са плоски нагреватели, които се поставят под стъклени терариуми/аквариуми или на стените им. Те са много икономични, имат много добри отоплителни свойства, имат различен размер и мощности. Препоръчително е тези нагреватели да се използват само от външната част на стъклени терариуми/аквариуми, или ако се ползват от вътре, което ние не препоръчваме, да бъдат много добре изолирани от животното. Това са нагревателни елементи, имат 220 волта захранване и не е препоръчително животното ви да има какъвто и да е достъп до тях.

![Нагревателен Кабел](@assets/custom/heat_cables/heat_cable_set.jpg)

#### Нагревателен кабел
Това са дълги кабели които отделят топлина и са идеални за отопляване на система от терариуми за масово отглеждане. Принципа им на работа е като килимчетата и предимствата и недостатъците са подобни.

![Купул за терариум::възможност за крушки и керамични нагреватели](@assets/guides/boa/glowlight.jpg)

#### Куполи

Тези купили могат да се ползват както с крушки така и с керамични нагреватели. Те са използват предимно в терариуми с мрежести покриви, аквариуми, или дървени терариуми където не позволява монтажа на фасунга вътре в терариума, като такива за дървесни видове.

![Лампа с нажежаема жичка::подходяща за отопляване само през деня](@assets/custom/basking_bulb.jpg)

#### Лампи с нажежаема жичка

Това са от лампите, с които си осветяваме стаите до по-специализирани рефлекторни лампи. Като цяло те отделят както топлина, така и светлина което е един от минусите им. За да отопляват те трябва да работят и нощем, което означава че ще пречат на животното да спи нощем. Трябва да се предоставя на боата 12 часа светлина и 12 часа тъмнина. Ако решите все пак да използвате такива нагреватели, то бъдете сигурни че нощем се изключват, и има друг нагревател който да поддържа температурата в терариума.

![Керамичен нагревател::подходящ за отопление по всяко време на деня](@assets/custom/ceramic/Ceramic_heater.jpg)

#### Керамични нагреватели

Това са нагреватели които не отделят светлина. Те се монтират като обикновени крушки с нажежаема жичка, но са много по-ефективни. Животът им е много по-дълъг от този на лампите, нагревателните им способности са много по-добри и могат да се ползват както денем така и нощем. Повърхността на тези нагреватели става много гореща затова трябва задължително да ги защитите от животното си. Това се прави най-лесно с метална мрежа около нагревателя. Тя не се нагрява защото нагревателя отделя инфрачервени лъчи, които не нагряват метала. Тези нагреватели са най-подходящ вариант за дървени терариуми.

#### Нагревателни килими

![Нагревателен камък](@assets/guides/boa/heatrock.jpg)

Това са нагреватели от вида на керамичните нагреватели, с изключението че се слагат до змията и тя се увива около тях. Тези нагреватели са виновни за множество изгаряния по множество животни по света и ние препоръчваме да стоите далеч от тях. Въпрос на време е кога вашето животно ще се изгори даже и по-лошо, защото като изгорят тези елементи работят на максимум, а змиите не могат да разберат кога се изгарят и сериозно могат да се наранят

## Аксесоари 

### Термометър:

![Дигитален Термометър::Цялостен вид](http://terrariums.eu/e107_plugins/easyshop/product_images_acc/sku_36137_3.jpg)

Задължително трябва да имате поне  1 термометър в терариума за да знаете колко градуса е в топлата част на терариума. Новите терморегулатори имат дисплей който отчита в реално време температурата в топлата част, което замества един термометър, позволявайки ви да имате само 1 с който да измервате студената част на терариума.


### Терморегулатор:

![Терморегулатор за терариум](http://terrariums.eu/e107_plugins/easyshop/product_images_acc/thermostat/1.jpg)

Това устройство се ползва за регулиране на нагревателя. След настройка на температурата която регулатора трябва да поддържа и включване на нагревателя към него, той започва да отчита температурата в терариума посредством сонда която се слага в топлата част на терариума. При достигане на желаната температура, регулатора изключва нагревателя. При изстиване в рамките на 1 градус или колкото сте задали на регулатора, той включва нагревателя отново. Това устройство е препоръчително, даже задължително за всеки един терариум, за да гарантирате точни температури вътре в него.


#### Влагомер:

![Термометър Влагомер/хигрометър](http://terrariums.eu/e107_plugins/easyshop/product_images_acc/thermom_hygro/thermo_hygro.jpg)

този аксесоар не е задължителен, но много хора предпочитат да знаят каква е влагата в терариума им, за да могат да я поддържат точно. В повечето случаи голяма купа под/над нагревателя ще поддържа влагата в терариума в прилични стойности и някое и друго напръскване с вода ще поддържа по-високи стойности.

## Осветление

Боите не се нуждаят от специално осветление. Те взимат всички нужни за тях хранителни вещества от храната си. Светлината която минава през прозореца на стаята и влиза в терариума е достатъчна за тези животни. Осветлението вътре в терариума е само за да виждаме животните в цялата им прелест. 

### Лампи с нажежаема жичка

Това са лампите с които също може да отоплявате терариума. Те се нагряват много, затова трябва да ограничите достъпа на змията до тях. Има голям избор от лампи, обикновени, халогенни, рефлекторни, червени  и черни (за нощта). 


### Флуоресцентни

Най-широко използвани за осветление на терариуми за змии. Тези лампи не отделят топлина, затова са по-безопасни за животното. Изключвайте ги нощем, както всички други осветителни тела.


### Пълен спектър

Това са УВА и УВБ лампи, който симулират част от лъчите на слънцето. Въпреки че не са нужни за змиите, те могат да имат някаква минимална полза от тях.

## Субстрат

Субстрат е постелката на дъното на терариума. Той трябва да се почиства редовно, за да се предотвратят болести по животните. Ще изредим няколко от най-използваните постелки за бои, и ще оставим на Вас да прецените кои ще са най-подходящи за Вас. Не оставяйте субстрата подгизнал, леко напръскване е добре но не повече. Това води до болести по животните, люспите им и др.

### Вестник/хартия
Това е може би най-евтиният вариант за субстрат възможен. Всички имаме вестници брошури от различните големи вериги магазини в страната. Много бързо се почистват, просто махате мръсният почиствате набързо пода, и слагате новият. Минуса е че не е никак естествено и красиво. 

### Стърготини
Това е другият широко използван субстрат. Аспен(трепетлика), Топола и други видове тополови стърготини са добър избор за постилка на терариума. Те не се чистят толкова лесно като вестника, но изглеждат и миришат много по-приятно.

В никакъв случай не ползвайте стърготини от иглолистни дървета. Те имат смоли и масла които са вредни за змията ви. Друг проблем е че ако храните змията вътре върху стърготините, тя може да погълне част от тях и те да причинят проблеми. Лесно решение е да сложите един вестник над стърготините докато животното си изяде плячката, след това го махнете. 

### Натурални Продукти
Това са всякакви типове субстрати от кори на дървета, натрошени кокосови черупки, които се предлагат от фирмите за аксесоари за влечуги. Имат характеристики като Стърготините, и подобни минуси. Не хранете животното директно върху тях за да не ги погълнат по време на хранене. Подмяната им в повече терариуми излиза малко по-скъпа от другите видове постелки.

### Алтернативи
Други алтернативи за постилане са изкуствена трева и други подобни настилки. Когато едно се измърси, слагате чистото, а мръсното го слагате за пране. Ние не препоръчваме такъв тим настилка за бои. Друг вариант са надробени кочани от царевица. Те мухлясват бързо и не са също препоръчителни. 

## Укрития

![Укритие подходящо за Боа](@assets/guides/boa/boa_hide.jpg)

Това е задължително за малките бои. Те имат нужда да се крият когато се почувстват застрашени. Трябва да им предоставите две укрития, в топлата и студената част, за да могат да се крият и в двата края. Такива укрития могат да се направят от множество неща, от кутии за обувки и пластмасови кутии от храна и сладолед, укрития имитиращи скали и камъни до купешки укрития специално за змии и влечуги. 

## Хранене

Змиите както знаем са хищници, това означава че се хранят с месо. В природата те са хищници от засада и дебнат плячката им да мине покрай тях, след което я захапват и удушават. В домашни условия ние ще сме тези които трябва да им даваме плячка, под жива или вече размразена форма. Лично наш фаворит за храна за бои са плъховете, но следните животни също стават за храна за вашата змия: мишки, плъхове, джербили, морски свинчета, кокошки и зайци. Правилото за хранене е че плячката не трябва да е по-дебела от най-дебелата част на змията. Малките змии трябва да бъдат хранене с малки мишлета. Ако им се дават прекалено големи, те ще повърнат , водейки до други проблеми, даже и синдром на повръщане.

По-големите змии могат да изядат и по няколко големи плъха, затова един заек би бил по-добър вариант за храна на големите екземпляри. 

> Силно препоръчваме храненето с прясно убити или размразени животни. Даването на живи животни може да доведе до много опасни ухапвания по вашата змия. Едно уплашено и притиснато животно ще хапе и дере за да се освободи от змията, причинявайки множество рани на змията. 

### Колко често да храня змията си?

Хранете с едно животно(мишка/плъх) на хранене, това ще гарантира дълъг и здрав живот на влечугото. Боите не трябва да се прехранват. Прехранването се получава когато се дават прекалено големи гризачи или се тъпче змията по време на хранене с втори или трети гризач, карайки я да изяде и тях. Това се прави за да ускори техният растеж и началният праг за репродукция, но крайно скъсява живота на змиите, и тези животни не живеят повече от 5 години. Една боа може да живее над 20 години ако се храни правилно. 


## Примерна таблица за хранене

|възраст|описание|
|--- |--- |
|Бебета бои (до 50 см) – до 3 месечна възраст :|може да се хранят с розови плъхчета или малки мишлета. Може да им се дава по едно на всеки 5 дена. Като отраснат и стигнат към 2 месечна възраст може да давате окосмени плъхчета, които са около 5-6 см.|
|Юноши (до около 60-90 см) от 3 до 1 годишна възраст :|се хранят с окосмени мишлета като към 10 месечна възраст с отбити плъхчета (10-11 см/2-3 седмични плъхчета) на всеки 7 дена.|
|Едно годишни( до около 1. метра) от 1 до 2 годишна възраст:|се хранят с малки до средни плъхове (11-20 см плъхове/6-8 седмични) веднъж на 10-14 дена.|
|Възрастни бои (над метър и половина):|се хранят с големи и ХL плъхове в зависимост от големината и дебелината на самата змия. Тези плъхове са около 20-25 см. Ако змията е много голяма, може да се дават зайци или да се сменят зайци и плъхове за разнообразие.|


![Таблица с размери на плъхове/мишки](@assets/guides/boa/MouseRatChart.jpg)

### Няколко основни насоки:

Никога не пипайте храната и после змията, просите си ухапване, защото миришете на храна за змията.

Оставяйте храната да се размрази през нощта на стайна температура или на парното докато сте сигурни ще е напълно размразена.


 Не хранете с жива храна! Описахме опасностите по-отгоре, тези животни са уплашени, хапят и често се виждат обезобразени змии заради такива ентусиасти които искат да гледат как змията им се справя с плячката.
 

## Вода

Достъпа до прясна вода е важен за вашата боа. Трябва да предоставите тежка купа с вода в терариума , тези купи трябва да са направени от керамика или друг тежък материал, за да не я бутне и разлее змията. Трябва да е достатъчно голяма, за да може змията да влезе и да се накисне в нея. Младите екземпляри имат навика да се киснат често във водата си. Трябва често да проверявате водата, защото боите имат навик да се изхождат в нея. Ако терариума позволява, е добре да има повече от една купа. Една в студената част за пиене, и една в топлата за да се изпарява и да поддържа влажността в терариума. Ако боата ви постоянно стои в купата с вода това може да значи че има здравословен проблем или и е много горещо в терариума.

## Влага

Влажността на терариума трябва да е подходяща за да няма вашето животно здравословни проблеми. Ако е твърде ниска, животното ще има проблеми при смяна на кожата, ако е твърде висока, може да се образува мухъл който да разрасте към люспите им, да им причини респираторни проблеми и др. Влажността трябва да е в рамките на 50-60 %, като в периода на смяна на кожата 70-80%. Поддържането на тези нива много зависи от вида на терариума, настилката и купите за вода в терариума. Широка купа с вода в топлата част над/под нагревателя ще осигури изпаряване на вода и ще предоставя естествено повишение на влагата в терариума.


Друг начин за влияние върху нивото на влажност е субстрата, вестника не задържа особено количество влажност, за разлика от кокосовите стърготини и корите от дървета. Те са естествени гъби и попиват и отдават влага равномерно и постепенно. За повишаване по време на смяна, може да пръскате животното със спрей с топла вода! Водата трябва да е топла защото като излезе от дифузера тя моментално изстива и така може да се стресира животното. 


## Витамини

Боите не се нуждаят от специални витамини. Те набавят всичко нужно от плячката си. Плъховете имат много богат на желязо черен дроб както и други вещества важни за здравето на змията.

## Поддръжка на терариума:

Боите не изискват много време и внимание, почистването на терариума обаче трябва да става възможно най-бързо след изхождането на животното. Оставянето на фекалии и урина в терариума може да доведе до развитието на бактерии и зарази опасни за животното. Почистването става чрез изгребване на замърсения субстрат (ако е настилка/вестник се сменя цялото парче) почистване с лек разтвор от вода и много малко препарат, може и за прозорци, и слагането на пресен субстрат.


## Хибернация
Тези змии не се нуждаят от хибернация, освен ако няма да ги размножавате. Това означава че може да поддържате нормални температури на терариума целогодишно. Ако решите да размножавате боите си температурата трябва да спадне до 21 градуса за около месец. Но това ще обсъдим в друг наръчник.


## Болести

Боите рядко се разболяват, главни причини за появата на болести са неправилни условия за живот, като температура влажност и тн. Ако животното ви изглежда болно и летаргично, опитайте да повишите температурите до 31 градуса в терариума, това би помогнало на змията ви да се възстанови по-бързо.

 Ако влажността в терариума е много ниска змията ще има проблеми при смяната на кожа, ако е твърде висока ще помага развитието на гъбички и бактерии по змията. 
 Ако змията стои постоянно във водата това може да е индикация за много високи температури, ниска влажност в терариума, или наличието на малки паякообразни кърлежи по змията. Те са много малки черни точици които ще забележете във водата ако змията се кисне постоянно. Ако това е проблема, закарайте животното на ветеринар незабавно.

## Смяна на кожата:

Тези животни никога не спират да растат, това означава че докато са живи ще сменят кожа, защото тя им става тясна и трябва да я сменят. Младите животни сменят веднъж месечно, възрастните с годините все по-рядко, през няколко месеца. За да осигурите правилна смяна на кожата трябва да са налице следните фактори:

![Потъмняване на очите при смяна](@assets/guides/boa/shed.jpg)

1.Избледняване на очите- това е сигурен знак, че змията ще сменя кожа. 
2.Посивяване на шарките – това подобно на очите е знак че змията може да сменя кожа.
3.Отказване да се храни –това понякога е знак че змията ще сменя кожа.

Ако са налице тези симптоми трябва да започнете да пръскате змията за да осигурите смяна от на кожата на цяло парче.


### Етапи на смяна

![Посивяли/посиняли очи на боа през смяна](@assets/guides/boa/blue_eyes.jpg)

Побеляване на очите – 2-3 дни (тогава змията не вижда добре и може да прояви агресия, това е момента, в който трябва да повишите влажността.

![Изчистване на очите - 2 фаза](@assets/guides/boa/after_blue.jpg)

Изчистване на очите – тогава очите се избистрят но змията все още не е сменила. Влажността трябва да продължава да е висока.

![Смяна на кожата - 3 фаза](@assets/guides/boa/shedding.jpg)

Смяна на кожата – змията сменя кожата си, влагата може да се върне в нормални стойности. 

![Смяна на кожата 3 фаза](@assets/guides/boa/shed-during.jpg)

Когато змията смени кожата си, развийте я и огледайте парчето откъм главата. Там трябва да има две люспички, които се отличават от всички други. Това са очните люспи. Ако те не се сменят с кожата, може да станат много сериозни усложнения при животното.

Ако змията ви има останали парчета кожа след смяна, може да я сложите в леген с топла вода. Така кожата ще омекне и може животното лесно да я свали. Друг вариант е да сложите влажен парцал под/над нагревателя в терариума, това ще повиши драстични влагата за момента и животното ще смени останалата кожа.
 
 Не дърпайте кожата сами! Животното само ще си я свали, трябва само да има нещо в което да се отърка в терариума. 
