---
titleОтглеждане на Бразилска Дъгова Боа
---
# Кратък наръчник за отглеждането на Бразилска Дъгова Боа

![Бразилска Дъгова Боа](@assets/guides/BRB/brb_.jpg)

## Въведение
[[toc]]

**Латинско Наименование:** Epicrates cenchria cenchria

Бразилските дъгови бои имат наситена червено/оранжева окраска с много красиви шарки. Името им идва от факта че люспите им пречупват светлината, пресъздавайки цветовете на дъгата.

## Характер
Тези змии са много спокойни и са добър избор за домашен любимец.
Малките екземпляри са е възможно да прояват агрсия, но това е защитен механизъм, който изчезва като порастнат.

## Размери
Обикновено Бразилските Дъгови Бои достигат размери между 1.20м и 1.6м, като в редки случаи женските екземпляри могат да нахдвърлят 1.8м. 
Грижата за тези животни не е трудно въпреки размерите им, те са по-финни от другите видове Бои.

## Субстрат и Вода
За постелка на пода може да ползвате различни продукти от разнообразието което се предлага на пазара за змии. 

> Стърготини от Бор и Кедър трябва да се избягват защото са токсични за змиите. 

Вестник и кухненска хартия са евтин избор, но не придават естествен вид на терариумът. Дървесните кори са също добър избор за субстрат, изглеждат добре, не са скъпи и задържат добре влажността в терариумът. 

Трябва да предоставите достатъчно голяма купа, в която змията да може да влиза и да се кисне. Това ще ѝ помогне при смяна на кожа, ако влажността в терариумът е не достатъчна.

## Осветление
Бразилските Дъгови Бои не се нуждаят от специално осветление, но ако решите да използвате такова за да виждате по-лесно животното и терариумът, бъдете сигурни че животното е предпазено от топлината която излъчва осветителното тяло, и неможе да се докосне до него.

![Бразилска дъгова боа](@assets/guides/BRB/brb1.jpg) 

## Отопление и Влажност
Бразилската Дъгова Боа се чувства най-добре между 28 и 30 градуса. Тази температура може да стигне до 26 в студената част, а през нощта до 22 градуса. 

Този вид Бои не понасят дълго излагане на високи температури, затова трябва да им предоставите този температурен градиент (температурна разлика).
 
### Често използвани нагреватели

* Нагревателни килимчета
* Нагревателни Кабели
* Нагревателни Лампи
* Керамични Нагреватели

Независимо, кой нагревател изберете, важно е да използвате терморегулатор за да поддържате постоянна температура в терариума. 
Влажността на Бразилската Дъгова Боа трябва да е между 60 и 80 %. Позволете на терариумът да изсъхне леко преди да го напръскате отново. Влажността трябва да бъде повишавана когато Боата започне да си сменя кожата. 
Скривалище пълно с влажен мъх е хубаво да бъде предоставено в терариумът, за да може животното да се крие, и да си помага при смяна на кожата.

## Терариумът
Една възрастна Бразилска Дъгова Боа може да се отглежда в терариум с размери 120х60х60 см.
Малките екземпляри могат да живеят в пластмасови кутии със скривалища и добре затварящ се вентилиран капак. 
Хубаво е да имат 2 скривалища, едно в студената и едно в топлата част на терариумът.

## Храна
Тези бои се хранят предимно с мишки, по-големите екземпляри се хранят с големи мишки или средно големи плъхове. 

> Големината на храната се определя от дебелината на змията. 

Плячката на змията не трябва да е по-дебела от най-дебелата част на тялото на змията. Препоръчително е да се дават мъртви мишки, за да се избегат ухапвания и наранявания на змията от мишката.

### Периодичност на хранене
Младите Бои се хранят всяка седмица, големите възрастни екземпляри през седмица, в зависимост от големината на мишката.
Прекомерното хранене на змиите води до затлъстяване и може да доведе до здравословни проблеми.

## Поддръжка
Водата трябва да се сменя на всеки 2 3 дена. Субстратът трябва да се преглежда и почиства от отпадъци и екскременти редовно. 
Пълно почистване на терариумът трябва да се прави на всеки 1 - 2 месеца.
