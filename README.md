---
title: Наръчници за отглеждане на екзотични животни 
---

![Terrariums.eu](@assets/logo.png)

Terrariums.eu e архив от наръчници за отглеждане на екзотични животни, подходящи за начинаещи в хобито.

Сайта е със свободен достъп и може всеки да добави статия или да коригира грешна информация като посети Bitbucket хранилището - [https://bitbucket.org/dhristov/terrariumsv2/src](https://bitbucket.org/dhristov/terrariumsv2/src)
