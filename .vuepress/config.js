const { resolve } = require('path')

module.exports = {
  title: 'Terrariums.eu',
  description: 'Наръчници за отжледане на екзотични животни за начинаещи',
  head: [

    ['link', { rel: 'apple-touch-icon', sizes: '180x180', href: '/favicons/apple-touch-icon.png' }],
    ['link', { rel: 'icon', type: 'image/png', sizes: '32x32', href: '/favicons/favicon-32x32.png' }],
    ['link', { rel: 'icon', type: 'image/png', sizes: '16x16', href: '/favicons/favicon-16x16.png' }],
    ['link', { rel: 'manifest', href: '/favicons/site.webmanifest' }],
    ['link', { rel: 'mask-icon', href: '/favicons/safari-pinned-tab.svg', color: '#5bbad5' }],
    ['link', { rel: 'shortcut icon', href: '/favicons/favicon.ico' }],
    ['meta', { name: 'msapplication-TileColor', content: '#ffffff' }],
    ['meta', { name: 'msapplication-config', content: '/favicons/browserconfig.xml' }],
    ['meta', { name: 'theme-color', content: '#ffffff' }]
  ],
  themeConfig: {
    nav: [{ text: 'Начало', link: '/' }, { text: 'Къде е Terrariums.eu?', link: '/about' }],
    sidebar: [
      {
        title: 'Наръчници',
        children: [
          ['/', 'Начало'],
          ['/guides/chameleon', 'Отглеждане на Хамелеон'],
          '/guides/leopard-gecko',
          '/guides/leopard-gecko-advanced.md',
          ['/guides/bmks', 'Отглеждане на Черна Мексиканска Змия'],
          ['/guides/brazillian-rainbow-boa', 'Отглеждане на Бразилска Дъгова Боа'],
          '/guides/iguana',
          '/guides/corn-snake',
          '/guides/bci.md',
          ['/guides/royal-python', 'Отглеждане на Кралски Питон'],
          '/guides/burm-python.md',
          '/guides/bearded-dragon.md',
          '/guides/crocodile-gecko.md',
          '/guides/mealworms.md'
        ]
      },
      {
        title: 'Помощ',
        children: [
          '/pages/faq.md',
          '/pages/vivarium_lighting.md',
          '/pages/vivarium_arrangement.md',
          '/pages/vivarium_heating.md',
          '/pages/bulb.md',
          '/pages/ceramic_heater.md',
          '/pages/heating_cable.md',
          '/pages/heating_matt.md',
        ]
      }
    ]
  },
  configureWebpack: {
    resolve: {
      alias: {
        '@assets': resolve(__dirname, '../assets')
      }
    }
  }
}
